import static java.lang.Math.*;

public class MultiThreadCalc extends Thread {
    private int threadNumber;
    private int nrThreads;
    private double dim;
    private static int[][] m1;
    private static int[][] m2;
    private static int[][] m3;
    public MultiThreadCalc(int dim,int nrThreads,int threadNumber, int[][]m1,int[][]m2,int[][]m3) {
        this.threadNumber=threadNumber;
        this.nrThreads=nrThreads;
        this.m1=m1;
        this.m2=m2;
        this.m3=m3;
        this.dim=dim;
    }

    @Override
    public void run() {

        double start=round(threadNumber*(dim/nrThreads));
        double end=round((threadNumber+1)*(dim/nrThreads));

        for (int i = (int) start; i <end ; i++) {
            for(int j=0;j<dim;j++)
                for(int k=0;k<dim;k++)
                m3[i][j]+=m1[i][k]*m2[k][j];

        }



    }


}
