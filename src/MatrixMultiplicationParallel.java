import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;


public class MatrixMultiplicationParallel implements ActionListener {

    private static int[][] m1;
    private static int[][] m2;
    private static int[][] m3;
    private static int dim;
    private static int nrThreads;
    private JLabel label_nr_threads;
    private JLabel label_dim_matrix;
    private JLabel label_setat;
    private JTextArea label_time;
    private JFrame frame;
    private JPanel panel;
    private JTextField number_threads;
    private JTextField dim_matrix;

    public MatrixMultiplicationParallel() {
        frame = new JFrame();

        frame.setSize(new Dimension(1000,600));
        panel = new JPanel();
        panel.setSize(new Dimension(400,400));
        JButton button_set = new JButton("Calculeaza");
        number_threads = new JTextField(40);
        dim_matrix = new JTextField(40);

        button_set.addActionListener(this);
        label_nr_threads = new JLabel("Numarul de threaduri este:");
        number_threads.setSize(new Dimension(20000,300));
        label_dim_matrix = new JLabel("Dimensiunea matricilor este:");
        label_setat = new JLabel();
        label_time= new JTextArea();





        panel.setBorder(BorderFactory.createEmptyBorder(30, 30, 10, 10));
        panel.setLayout(new GridLayout(6, 1));
        panel.add(label_nr_threads);
        panel.add(number_threads);
        panel.add(label_dim_matrix);
        panel.add(dim_matrix);
        panel.add(button_set);
        panel.add(label_setat);

        JPanel panel2 = new JPanel();
        panel2.add(label_time);

        label_time.setRows(25);
        label_time.setColumns(25);
        label_time.setLineWrap(true);
        label_time.setEditable(false);


        frame.add(panel, BorderLayout.PAGE_START);
        frame.add(panel2,BorderLayout.PAGE_END);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setTitle("Multilpication of matrix Parallel");
        frame.pack();
        frame.setVisible(true);



    }


    @Override
    public void actionPerformed(ActionEvent e) {

        dim = Integer.parseInt(dim_matrix.getText());
        nrThreads = Integer.parseInt(number_threads.getText());
        label_setat.setText("Inmultim paralel o matrice de dimensiunea: " + dim + " cu un numar de " + nrThreads + " threaduri");
        generare_matrici();

        ArrayList<MultiThreadCalc> list= new ArrayList<>();
        Date startd = new Date();
        for (int i = 0; i < nrThreads; i++) {

            MultiThreadCalc t1 = new MultiThreadCalc(dim, nrThreads, i, m1, m2, m3);
            list.add(t1);
            t1.start();

        }
        for (MultiThreadCalc it:list
             ) {
            try {
                it.join();
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }

        }
        Date endd = new Date();

        String final_time =("Timpul in miliseconds cu "+nrThreads+" threaduri: " + (endd.getTime() - startd.getTime()));
        label_time.setText(label_time.getText()+ final_time+ "\n");

    }

    public static int[][] generateMatrix(int rows, int columns) {

        int[][] result = new int[rows][columns];
        Random random = new Random();

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                result[i][j] = random.nextInt(10000) ;
            }
        }
        return result;
    }

    public static void print(int[][] matrix) {

        System.out.println();

        int columns = matrix[0].length;
        for (int[] ints : matrix) {
            for (int j = 0; j < columns; j++) {
                System.out.print(ints[j] + "  ");
            }
            System.out.println();
        }

    }

    public static void generare_matrici() {

        m1 = generateMatrix(dim, dim);
        m2 = generateMatrix(dim, dim);
        m3 = new int [dim][dim];
//        System.out.print("Matricea m1 este: ");
//        print(m1);
//        System.out.println();
//        System.out.print("Matricea m2 este: ");
//        print(m2);
//        System.out.println();

    }

    public static void main(String[] args) throws InterruptedException {

        new MatrixMultiplicationParallel();

    }

}